function nhapThongTin() {
  var taiKhoan = document.getElementById("tknv").value.trim();
  var tenNV = document.getElementById("name").value.trim();
  var email = document.getElementById("email").value.trim();
  var matKhau = document.getElementById("password").value.trim();
  var date = document.getElementById("datepicker").value.trim();
  var luongCB = document.getElementById("luongCB").value * 1;
  var chucVuCty = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value * 1;
  var nv = new NhanVien(
    taiKhoan,
    tenNV,
    email,
    matKhau,
    date,
    luongCB,
    chucVuCty,
    gioLam
  );
  return nv;
}

function renderTable(list) {
  var contentHTML = "";
  list.forEach(function (item) {
    var content = `<tr>
    <td>${item.taiKhoan}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td>${item.ngay}</td>
    <td>${item.chucVu()}</td>
    <td>${new Intl.NumberFormat("vi-VN").format(item.tinhTongTien())} VND </td>
    <td>${item.tinhGioLamNV()}</td>
    <td>
    <button onclick="xoaNV('${
      item.taiKhoan
    }')" class="btn btn-primary mb-2">Xoá</button>
    <button onclick="suaNV('${
      item.taiKhoan
    }')" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Sửa</button>
    </td>
    </tr>`;
    contentHTML += content;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
  // for (var index = 0; index < arr.length; index++) {
  //   var sv = arr[index];
  //   if (sv.taiKhoan == id) {
  //     return index;
  //   }
  // }
  // return -1;

  return arr.findIndex(function (nv) {
    return nv.taiKhoan == id;
  });
}
function showThongTin(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.pass;
  document.getElementById("datepicker").value = nv.ngay;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucVuCty;
  document.getElementById("gioLam").value = nv.gioLam;
}
