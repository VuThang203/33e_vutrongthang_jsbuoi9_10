function NhanVien(
  taiKhoan,
  tenNV,
  email,
  matKhau,
  date,
  luongCB,
  chucVuCty,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.ten = tenNV;
  this.email = email;
  this.pass = matKhau;
  this.ngay = date;
  this.luong = luongCB;
  this.chucVuCty = chucVuCty;
  this.gioLam = gioLam;
  this.chucVu = function () {
    var tenGoi = "";
    if (this.chucVuCty == 1) {
      tenGoi = "Sếp";
    }
    if (this.chucVuCty == 2) {
      tenGoi = "Trưởng phòng";
    }
    if (this.chucVuCty == 3) {
      tenGoi = "Nhân viên";
    }
    return tenGoi;
  };
  this.tinhTongTien = function () {
    var tongLuong = 0;
    if (this.chucVuCty == 1) {
      tongLuong = this.luong * 3;
    }
    if (this.chucVuCty == 2) {
      tongLuong = this.luong * 2;
    }
    if (this.chucVuCty == 3) {
      tongLuong = this.luong;
    }
    return tongLuong;
  };
  this.tinhGioLamNV = function () {
    var ketQua = "";
    if (this.gioLam >= 192) {
      ketQua = "Nhân viên xuất sắc";
    } else if (this.gioLam >= 176) {
      ketQua = "Nhân viên giỏi";
    } else if (this.gioLam >= 160) {
      ketQua = "Nhân viên khá";
    } else {
      ketQua = "Nhân viên trung bình";
    }
    return ketQua;
  };
}
