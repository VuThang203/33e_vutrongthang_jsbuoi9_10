var validator = {
  kiemTraRong: function (valueInput, idError) {
    if (valueInput == "") {
      document.getElementById(idError).innerText = "Không được để rỗng!";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTRaDoDai: function (valueInput, idError, max) {
    var inputLength = valueInput.length;
    if (inputLength > max) {
      document.getElementById(idError).innerText = `Tối đa là ${max} ký số`;
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraTaiKhoanTrung: function (tkNV, dsnv) {
    var index = dsnv.findIndex((nv) => {
      return nv.taiKhoan == tkNV;
    });
    if (index !== -1) {
      document.getElementById("tbTKNV").innerText = "Tài khoản đã tồn tại!";
      document.getElementById("tbTKNV").style.display = "block";
      return false;
    } else {
      document.getElementById("tbTKNV").innerText = "";
      return true;
    }
  },
  kiemTraEmail: function (valueInput, idEmail) {
    var validRegex =
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (validRegex.test(valueInput)) {
      document.getElementById(idEmail).innerHTML = "";

      return true;
    } else {
      document.getElementById(idEmail).innerHTML = "Email không hợp lệ!";
      document.getElementById(idEmail).style.display = "block";
      return false;
    }
  },
  kiemTraTen: function (valueInput, idName) {
    var name =
      /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$/;
    if (name.test(valueInput)) {
      document.getElementById(idName).innerText = "";
      return true;
    } else {
      document.getElementById(idName).innerHTML = "Tên không hợp lệ!";
      document.getElementById(idName).style.display = "block";
      return false;
    }
  },
  kiemTraMatKhau: function (valueInput, idPass) {
    var pass = /^(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,10}$/;
    if (pass.test(valueInput)) {
      document.getElementById(idPass).innerText = "";
      return true;
    } else {
      document.getElementById(idPass).innerText =
        "Mật khẩu 6-10 ký tự (1 ký tự số, 1 ký tự hoa, 1 ký tự đặc biệt)";
      document.getElementById(idPass).style.display = "block";
      return false;
    }
  },
  kiemTraNgayThangNam: function (valueInput, idNgay) {
    var ktNgay = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    if (ktNgay.test(valueInput)) {
      document.getElementById(idNgay).innerText = "";
      return true;
    } else {
      document.getElementById(idNgay).innerText =
        "Vui lòng nhập theo định dạng [mm/dd/yyyy]";
      document.getElementById(idNgay).style.display = "block";
      return false;
    }
  },
  kiemTraLuongCoBan: function (valueInput, idLuong, min, max) {
    var ktLuong = valueInput.length;
    if (ktLuong > max || ktLuong < min) {
      document.getElementById(
        idLuong
      ).innerText = `Lương cơ bản phải từ ${new Intl.NumberFormat(
        "vi-VN"
      ).format(min)} vnd - ${new Intl.NumberFormat("vi-VN").format(max)} vnd`;
      document.getElementById(idLuong).style.display = "block";
      return false;
    } else {
      document.getElementById(idLuong).innerText = "";
      return true;
    }
  },
  kiemTraChucVu: function (valueInput, idChucVu) {
    if (valueInput == 1 || valueInput == 2 || valueInput == 3) {
      document.getElementById(idChucVu).innerText = "";
      return true;
    } else {
      document.getElementById(idChucVu).innerText = `Vui lòng chọn chức vụ`;
      document.getElementById(idChucVu).style.display = "block";
      return false;
    }
  },
  kiemTraSoGioLam: function (valueInput, idGio) {
    if (valueInput < 80 || valueInput > 200) {
      document.getElementById(idGio).innerText = `Số giờ làm từ 80 - 200 giờ`;
      document.getElementById(idGio).style.display = "block";
      return false;
    } else {
      document.getElementById(idGio).innerText = "";
      return true;
    }
  },
};
function removeVietnameseTones(str) {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
  str = str.replace(/Đ/g, "D");
  // Some system encode vietnamese combining accent as individual utf-8 characters
  // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
  // Remove extra spaces
  // Bỏ các khoảng trắng liền nhau
  str = str.replace(/ + /g, " ");
  str = str.trim();
  return str;
}
