var dsnv = [];
const DSNV = "DSNV";

let dsnvLocalStorage = localStorage.getItem(DSNV);

// localstorage
if (JSON.parse(dsnvLocalStorage)) {
  var data = JSON.parse(dsnvLocalStorage);
  for (var index = 0; index < data.length; index++) {
    var current = data[index];
    var nv = new NhanVien(
      current.taiKhoan,
      current.ten,
      current.email,
      current.pass,
      current.ngay,
      current.luong,
      current.chucVuCty,
      current.gioLam
    );
    dsnv.push(nv);
  }
  renderTable(dsnv);
}
function saveLocalStorage() {
  var dsnvJson = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, dsnvJson);
}
// Thêm nhân viên
document.getElementById("btnThemNV").onclick = function () {
  var nvCapNhat = nhapThongTin();
  // validator taiKhoan
  var isValid =
    validator.kiemTraRong(nvCapNhat.taiKhoan, "tbTKNV") &&
    validator.kiemTRaDoDai(nvCapNhat.taiKhoan, "tbTKNV", 6) &&
    validator.kiemTraTaiKhoanTrung(nvCapNhat.taiKhoan, dsnv);
  // validator ten
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.ten, "tbTen") &&
      validator.kiemTraTen(nvCapNhat.ten, "tbTen"));
  // validator email
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.email, "tbEmail") &&
      validator.kiemTraEmail(nvCapNhat.email, "tbEmail"));
  // validator pass
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.pass, "tbMatKhau") &&
      validator.kiemTraMatKhau(nvCapNhat.pass, "tbMatKhau"));
  // validator date
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.ngay, "tbNgay") &&
      validator.kiemTraNgayThangNam(nvCapNhat.ngay, "tbNgay"));
  // validator luong
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.luong, "tbLuongCB") &&
      validator.kiemTraLuongCoBan(
        nvCapNhat.luong,
        "tbLuongCB",
        1000000,
        20000000
      ));
  // validator chuc vu
  isValid = isValid & validator.kiemTraChucVu(nvCapNhat.chucVuCty, "tbChucVu");
  // validator gio lam
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.gioLam, "tbGiolam") &&
      validator.kiemTraSoGioLam(nvCapNhat.gioLam, "tbGiolam"));
  if (isValid == false) {
    return;
  }
  dsnv.push(nvCapNhat);
  saveLocalStorage();
  renderTable(dsnv);
};

// Cập nhật nhân viên
document.getElementById("btnCapNhat").onclick = function () {
  var nvCapNhat = nhapThongTin();
  // validator taiKhoan
  var isValid =
    validator.kiemTraRong(nvCapNhat.taiKhoan, "tbTKNV") &&
    validator.kiemTRaDoDai(nvCapNhat.taiKhoan, "tbTKNV", 6);
  // validator ten
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.ten, "tbTen") &&
      validator.kiemTraTen(nvCapNhat.ten, "tbTen"));
  // validator email
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.email, "tbEmail") &&
      validator.kiemTraEmail(nvCapNhat.email, "tbEmail"));
  // validator pass
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.pass, "tbMatKhau") &&
      validator.kiemTraMatKhau(nvCapNhat.pass, "tbMatKhau"));
  // validator date
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.ngay, "tbNgay") &&
      validator.kiemTraNgayThangNam(nvCapNhat.ngay, "tbNgay"));
  // validator luong
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.luong, "tbLuongCB") &&
      validator.kiemTraLuongCoBan(
        nvCapNhat.luong,
        "tbLuongCB",
        1000000,
        20000000
      ));
  // validator chuc vu
  isValid = isValid & validator.kiemTraChucVu(nvCapNhat.chucVuCty, "tbChucVu");
  // validator gio lam
  isValid =
    isValid &
    (validator.kiemTraRong(nvCapNhat.gioLam, "tbGiolam") &&
      validator.kiemTraSoGioLam(nvCapNhat.gioLam, "tbGiolam", 80, 200));
  if (isValid == false) {
    return;
  }

  let index = timKiemViTri(nvCapNhat.taiKhoan, dsnv);
  if (index !== -1) {
    dsnv[index] = nvCapNhat;
    saveLocalStorage();

    renderTable(dsnv);
  }
};
// Sửa nhân viên
function suaNV(id) {
  var index = timKiemViTri(id, dsnv);
  if (index !== -1) {
    showThongTin(dsnv[index]);
  }
}

// Xoá nhân viên
function xoaNV(id) {
  var index = timKiemViTri(id, dsnv);
  if (index !== -1) {
    dsnv.splice(index, 1);
    saveLocalStorage();
    renderTable(dsnv);
  }
}

// Tìm kiếm nhân viên
function timKiemXL() {
  var giaTriTim = document.getElementById("searchName").value;
  giaTriTim = removeVietnameseTones(giaTriTim).toUpperCase();
  var danhSachTim = [];
  if (giaTriTim) {
    dsnv.forEach(function (item) {
      console.log("item: ", item);
      var soSanh = item.tinhGioLamNV();
      soSanh = removeVietnameseTones(soSanh).toUpperCase();
      if (soSanh.includes(giaTriTim)) {
        danhSachTim.push(item);
        // renderTable(danhSachTim);
      }
    });
    renderTable(danhSachTim);
  }
}
